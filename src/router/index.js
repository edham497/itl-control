import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Carga from "../views/Carga.vue";
import Login from "../views/Login.vue";
import Kardex from "../views/Kardex.vue";
import BajaMaterias from "../views/BajaMateria.vue";
import AltaMaterias from "../views/CargaMateria.vue";

Vue.use(VueRouter);

const routes = [
  { path: "/", name: "Horario", component: Home },
  { path: "/cargaAcademica", name: "Carga Academica", component: Carga },
  { path: "/login", name: "Login", component: Login },
  { path: "/kardex", name: "Kardex", component: Kardex },
  { path: "/baja", name: "Baja", component: BajaMaterias },
  { path: "/alta", name: "Alta", component: AltaMaterias },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const authRequired = to.path !== "/login";
  const loggedIn = JSON.parse(localStorage.getItem("$tkn"));
  // console.log(loggedIn)

  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && !loggedIn) {
    next("/login");
  } else {
    if (loggedIn && to.path == "/login") {
      next("/");
    } else next();
  }
});

export default router;
