const horario = JSON.parse(localStorage.getItem("$horario"));

export const Materias = {
  state: {
    $horario: horario ? horario : null,
  },
  mutations: {
    setHorario(state, data) {
        state.$horario = data.map((materia) => {
            return {
                Grupo: materia.Grupo,
                Materia: materia.Materia,
                Profesor: materia.Profesor,
                Dias: {
                    Lunes: materia.Lunes,
                    Martes: materia.Martes,
                    Miercoles: materia.Miercoles,
                    Jueves: materia.Jueves,
                    Viernes: materia.Viernes
                },
                isActive: false,
                isSelected: false
            }
        })
    },
  },
  actions: {
    async getHorario(context) {
        console.log(context)
    //   const response = await CFetch.get("api/v1/carga", getters.token);
    //   if (response != "ERROR") {
    //     setTimeout(() => {
    //       commit("setHorario", response);
    //     }, 250);
    //   }
    },
  },
};
