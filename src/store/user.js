import { CFetch } from "@/plugins/CFetch.js";

const user = JSON.parse(localStorage.getItem("$usr"));
const token = JSON.parse(localStorage.getItem("$tkn"));

export const User = {
  namespaced: true,
  state: {
    $usr: user ? user : null,
    $tkn: token ? token : null,
    horario: [],
  },
  mutations: {
    setUserInfo(state, info) {
      state.$usr = info;
      localStorage.setItem("$usr", JSON.stringify(info));
    },
    removeUserInfo(state) {
      state.$usr = null;
      localStorage.clear();
    },
    setToken(state, tkn) {
      state.$tkn = tkn;
      localStorage.setItem("$tkn", JSON.stringify(tkn));
    },
    setHorario(state, horario) {
      state.horario = horario.map((materia) => {
        return {
          Grupo: materia.Grupo,
          Materia: materia.Materia,
          Profesor: materia.Profesor,
          Dias: {
            Lunes: materia.Lunes,
            Martes: materia.Martes,
            Miercoles: materia.Miercoles,
            Jueves: materia.Jueves,
            Viernes: materia.Viernes,
          },
          isActive: false
        };
      });
    },
  },
  actions: {
    async getUserInfo({ commit, getters }) {
      const response = await CFetch.get("api/v1/user", getters.token);
      commit("setUserInfo", response);
    },
    async login({ commit }, data) {
      const response = await CFetch.post("api/signin", JSON.stringify(data));

      if (response != "ERROR") {
        commit("setToken", response.token);
        return true;
      } else return false;
    },
    async getHorario({ commit, getters }) {
      const response = await CFetch.get("api/v1/carga", getters.token);
      if (response != "ERROR") {
        setTimeout(()=>{
          commit("setHorario", response);

        }, 250)
      }
    },
    logout({ commit }) {
      commit("removeUserInfo");
    },
  },
  getters: {
    token: (state) => {
      return state.$tkn;
    },
    horario: (state) => {
      return state.horario
    }
  },
};
