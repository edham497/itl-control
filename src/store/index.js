import Vue from 'vue'
import Vuex from 'vuex'

import { User } from "./user"
import { Materias } from "./materias"

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    User,
    Materias
  }
})
