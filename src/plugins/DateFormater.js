export default new class {
    constructor(){
        this.date = new Date();
        // console.log(this.date.getMonth(), 
        // this.date.getDate(),
        // this.date.getHours() >= 12 ? this.date.getHours() - 12 : this.date.getHours(),
        // this.date.getMinutes() )
    }

    getDay(){
        switch (this.date.getDay()){
            case 0: return "Domingo"
            case 1: return "Lunes"
            case 2: return "Martes"
            case 3: return "Miercoles"
            case 4: return "Jueves"
            case 5: return "Viernes"
            case 6: return "Sabado"
        }
    }

    getMonth(){
        switch (this.date.getMonth()){
            case 0: return "Enero";
            case 1: return "Febrero";
            case 2: return "Marzo";
            case 3: return "Abril";
            case 4: return "Mayo";
            case 5: return "Junio";
            case 6: return "Julio";
            case 7: return "Agosto";
            case 8: return "Septiembre";
            case 9: return "Octubre";
            case 10: return "Noviembre";
            case 11: return "Diciembre";
        }
    }

    getFormatedDate(){
        return `${this.getDay()}, ${this.date.getDate()} de ${this.getMonth()}`
    }

    getHour(){
        return this.date.getHours()
    }
}