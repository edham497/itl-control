const BASE_URL = "https://tec-laguna-api.herokuapp.com/";

export class CFetch {
  static async post(url, data) {
    const request = await fetch(BASE_URL + url, {
      method: "POST",
      body: data,
      headers: {
        "Content-Type": "application/json"
      }
    });
    if (request.ok) {
      const response = await request.json();
      return response;
    } else {
      return "ERROR";
    }
  }
  static async get(url, token) {
    const request = await fetch(BASE_URL + url, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    const response = await request.json();
    return response;
  }
}
