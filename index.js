import express from "express";
import serveStatic from "serve-static";
import path from "path";
import history from "connect-history-api-fallback"

const app = express();
app.use(history())

app.use("/", serveStatic(path.join(__dirname, "/dist")));

app.set("port", process.env.PORT || 5000);

app.listen(app.get("port"), () => {
//   console.log("http://localhost:" + app.get("port"));
});
